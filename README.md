# Gametech-Deploy

The public deployment of a UT-GameTech project.

**INSTRUCTIONS:**

Linux: To run the game, use runner.x86\_64

Windows/Mac: Builds coming soon.

**CREDIT:

Programmers:

Calvin Dao

Edwin Silerio

Ian Thorne

Please note this is a Linux Build.
